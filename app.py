from flask import Flask, escape, request, render_template
import configparser
import sqlite3
config = configparser.ConfigParser()
config.read("configuration.ini")
app = Flask(__name__)

if config["db"]["type"] == "sqlite":
    db_conn = sqlite3.connect(
        f"{config['db']['path']}", check_same_thread=False)


def parseProject(sqlData, db=None):
    data = {
        'idprojets': sqlData[0],
        'nom': sqlData[1],
        'url': sqlData[2],
        'description': sqlData[3],
        'background_image_path': sqlData[4],
        'article': sqlData[5],
        'idclients': sqlData[6]
    }
    if db is not None:
        curs = db.cursor()
        curs.execute(
            """SELECT nom, website, description FROM `clients` WHERE idclients = ?;""", (data["idclients"],))
        client = curs.fetchone()
        data["client"] = {
            'nom': client[0],
            'website': client[1],
            'description': client[2]
        }

        curs.execute("""SELECT r.idrecomandations, r.titre, r.description, r.idclients, r.background_image_path, r.background_color 
                    FROM `recomandations` r,
                    clients c,
                    projets p
                    WHERE r.idclients = c.idclients AND p.idclients = c.idclients AND p.idprojets = ?;""", (data["idprojets"],))
        recommendation = curs.fetchone()
        if recommendation is not None:
            data["recomandation"] = {
                'idrecomandations': recommendation[0],
                'titre': recommendation[1],
                'description': recommendation[2],
                'idclients': recommendation[3],
                'background_image_path': recommendation[4],
                'background_color': recommendation[5]
            }
        curs.execute("""SELECT c.idcompetances, c.nom, c.url, c.description, c.icon_path
                        FROM    `competances` c,
                                competances_projets cp
                        WHERE c.idcompetances = cp.idcompetances AND cp.idprojets = ?;""", (data["idprojets"],))
        # Get Competances
        data["competances"] = []
        competance = curs.fetchone()
        while competance is not None:
            data["competances"].append(parseCompetance(competance))
            competance = curs.fetchone()
    return data


def parseCompetance(sqlData, db=None):
    output = {
        'idcompetances': sqlData[0],
        'nom': sqlData[1],
        'url': sqlData[2],
        'description': sqlData[3],
        'icon_path': sqlData[4],
    }
    if db is not None:
        curs = db.cursor()
        curs.execute("""SELECT 
                        p.idprojets, p.nom
                        FROM
                            projets p,
                            competances_projets cp
                        WHERE
                            p.idprojets = cp.idprojets AND cp.idcompetances = ?;""", (output["idcompetances"],))
        output["projets"] = []
        projet = curs.fetchone()
        while projet is not None:
            output["projets"].append({
                "idprojet": projet[0],
                "nom": projet[1]
            })
            projet = curs.fetchone()
    return output


def parseRecommandation(sqlData):
    return {
        'titre': sqlData[0],
        'description': sqlData[1],
        'background_image_path': sqlData[2],
        'background_color': sqlData[3],
        'client': {
            'nom': sqlData[4],
            'website': sqlData[5],
            'description': sqlData[6]
        }
    }


@app.route('/')
def index():
    data = {}
    data["title"] = config["website"]["title"]

    data["competances"] = []
    cursor = db_conn.cursor()

    # Get Competances
    cursor.execute("""SELECT idcompetances, nom, url, description, icon_path
                   FROM `competances`;
                   """)
    competance = cursor.fetchone()
    while competance is not None:
        data["competances"].append(parseCompetance(competance, db_conn))
        competance = cursor.fetchone()

    # Get projects
    cursor.execute("""SELECT idprojets, nom, url, description, background_image_path, article, idclients
                    FROM `projets`;""")
    data["projets"] = []
    projet = cursor.fetchone()

    while projet is not None:
        data["projets"].append(parseProject(projet))
        projet = cursor.fetchone()

    # Get recommandations
    cursor.execute(
        """SELECT r.titre, r.description, r.background_image_path, r.background_color , c.nom, c.website, c.description
FROM `recomandations` r, clients c
WHERE r.idclients = c.idclients;""")

    data["recommandations"] = []
    recommendation = cursor.fetchone()

    while recommendation is not None:
        data["recommandations"].append(parseRecommandation(recommendation))
        recommendation = cursor.fetchone()

    return render_template("index.html", data=data)


@app.route("/projects/<id>")
def project(id=None):
    curs = db_conn.cursor()
    curs.execute("""SELECT idprojets, nom, url, description, background_image_path, article, idclients
                    FROM `projets` WHERE idprojets = ? """, (id,))

    data = parseProject(curs.fetchone(), db_conn)
    return render_template("project.html", data=data)


@app.route("/contact")
def contact():
    data = {
        'title': config["website"]["title"]
    }

    return render_template("contact.html", data=data)


@app.route("/recommendation")
def recommendation():
    data = {
        'title': config["website"]["title"]
    }

    return render_template("recommendation.html", data=data)
