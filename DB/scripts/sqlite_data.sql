-- SQLite
-- Insert data into clients
INSERT INTO `clients` (nom, website, description)
VALUES
  (
    "Adobe Systems",
    "https://www.adobe.com/fr/",
    "Une super entreprise nommée Adobe"
  ),
  (
    "Advanced Micro Devices (AMD)",
    "https://www.amd.com/fr",
    "Une super entreprise nommée Advanced"
  ),
  (
    "Aetna",
    "https://www.aetna.com/",
    "Une super entreprise nommée Aetna"
  ),
  (
    "AGCO",
    "https://www.agcocorp.com/",
    "Une super entreprise nommée AGCO"
  ),
  (
    "Airborne Express",
    "https://en.wikipedia.org/wiki/Airborne_Express",
    "Une super entreprise nommée Airborne"
  ),
  (
    "Albertsons",
    "https://www.albertsons.com/",
    "Une super entreprise nommée Albertsons"
  ),
  (
    "Alcoa",
    "https://www.alcoa.com/global/en/home.asp",
    "Une super entreprise nommée Alcoa"
  ),
  (
    "Altria Group (anciennement Philip Morris Companies Inc.)",
    "https://www.altria.com/Pages/default.aspx",
    "Une super entreprise nommée Altria"
  ),
  (
    "Amazon.com",
    "http://amazon.com",
    "Une super entreprise nommée Amazon"
  ),
  (
    "American Airlines",
    "https://www.americanairlines.fr",
    "Une super entreprise nommée American"
  );
-- SQLite
  -- Insert data into recomandations
INSERT INTO `recomandations` (
    titre,
    description,
    idclients,
    background_image_path,
    background_color
  )
VALUES
  (
    "Un super développeur",
    "Nous a fourni un excellent travail durant toute notre collaboration",
    1,
    "https://pbs.twimg.com/media/DoQGxT6W0AAPred.png",
    "#e2aea1"
  ),
  (
    "De bon conseils",
    "Nous a fourni des conseils avisés",
    2,
    "https://pbs.twimg.com/media/DoQGxT6W0AAPred.png",
    "#e2aea1"
  ),
  (
    "Un bon élement",
    "Moteur et dynamique",
    3,
    "https://pbs.twimg.com/media/DoQGxT6W0AAPred.png",
    "#e2aea1"
  ),
  (
    "Acteur majeur",
    "Force de proposition tout au long de son expérience chez nous",
    4,
    "https://pbs.twimg.com/media/DoQGxT6W0AAPred.png",
    "#e2aea1"
  ),
  (
    "Un dev au top",
    "Il est trop cool et efficace 😎",
    5,
    "https://pbs.twimg.com/media/DoQGxT6W0AAPred.png",
    "#e2aea1"
  );
-- SQLite
  -- Insert data into recomandations
INSERT INTO `projets` (
    nom,
    url,
    description,
    background_image_path,
    article,
    idclients
  )
VALUES
  (
    "Un projet de ouf en Python",
    "https://fr.wikipedia.org/wiki/Projet",
    "Ce projet premet plein de choses super comme Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rhoncus mauris sed fringilla consequat. Integer sed lobortis purus. Nam maximus eros id fringilla interdum. Suspendisse porta suscipit orci, auctor laoreet magna ultricies id. Aliquam erat volutpat. Donec lorem neque, laoreet nec commodo et, dapibus in est. Nam elit massa, imperdiet sed porta euismod, dignissim eget est. ",
    "https://upload.wikimedia.org/wikipedia/commons/f/f8/Python_logo_and_wordmark.svg",
    '<h1>HTML Ipsum Presents</h1>

<p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>

<h2>Header Level 2</h2>

<ol>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ol>

<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote>

<h3>Header Level 3</h3>

<ul>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ul>

<pre><code>
#header h1 a {
  display: block;
  width: 300px;
  height: 80px;
}
</code></pre>',
    1
  ),(
    "Un projet de ouf en Java",
    "https://fr.wikipedia.org/wiki/Projet",
    "Ce projet premet plein de choses super comme Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rhoncus mauris sed fringilla consequat. Integer sed lobortis purus. Nam maximus eros id fringilla interdum. Suspendisse porta suscipit orci, auctor laoreet magna ultricies id. Aliquam erat volutpat. Donec lorem neque, laoreet nec commodo et, dapibus in est. Nam elit massa, imperdiet sed porta euismod, dignissim eget est. ",
    "https://upload.wikimedia.org/wikipedia/fr/2/2e/Java_Logo.svg",
    '<h1>HTML Ipsum Presents</h1>

<p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>

<h2>Header Level 2</h2>

<ol>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ol>

<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote>

<h3>Header Level 3</h3>

<ul>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ul>

<pre><code>
#header h1 a {
  display: block;
  width: 300px;
  height: 80px;
}
</code></pre>',
    2
  ),(
    "Un projet de ouf en SQL",
    "https://fr.wikipedia.org/wiki/Projet",
    "Ce projet premet plein de choses super comme Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rhoncus mauris sed fringilla consequat. Integer sed lobortis purus. Nam maximus eros id fringilla interdum. Suspendisse porta suscipit orci, auctor laoreet magna ultricies id. Aliquam erat volutpat. Donec lorem neque, laoreet nec commodo et, dapibus in est. Nam elit massa, imperdiet sed porta euismod, dignissim eget est. ",
    "https://wikimedia.org/api/rest_v1/media/math/render/svg/b83ad563285f7b0ebb325226d91f25ca0bffa7cd",
    '<h1>HTML Ipsum Presents</h1>

<p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>

<h2>Header Level 2</h2>

<ol>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ol>

<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote>

<h3>Header Level 3</h3>

<ul>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ul>

<pre><code>
#header h1 a {
  display: block;
  width: 300px;
  height: 80px;
}
</code></pre>',
    3
  ),(
    "Un projet de ouf en C",
    "https://fr.wikipedia.org/wiki/Projet",
    "Ce projet premet plein de choses super comme Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rhoncus mauris sed fringilla consequat. Integer sed lobortis purus. Nam maximus eros id fringilla interdum. Suspendisse porta suscipit orci, auctor laoreet magna ultricies id. Aliquam erat volutpat. Donec lorem neque, laoreet nec commodo et, dapibus in est. Nam elit massa, imperdiet sed porta euismod, dignissim eget est. ",
    "https://upload.wikimedia.org/wikipedia/commons/3/35/The_C_Programming_Language_logo.svg",
    '<h1>HTML Ipsum Presents</h1>

<p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>

<h2>Header Level 2</h2>

<ol>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ol>

<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote>

<h3>Header Level 3</h3>

<ul>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ul>

<pre><code>
#header h1 a {
  display: block;
  width: 300px;
  height: 80px;
}
</code></pre>',
    4
  ),(
    "Un projet de ouf en Rust",
    "https://fr.wikipedia.org/wiki/Projet",
    "Ce projet premet plein de choses super comme Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rhoncus mauris sed fringilla consequat. Integer sed lobortis purus. Nam maximus eros id fringilla interdum. Suspendisse porta suscipit orci, auctor laoreet magna ultricies id. Aliquam erat volutpat. Donec lorem neque, laoreet nec commodo et, dapibus in est. Nam elit massa, imperdiet sed porta euismod, dignissim eget est. ",
    "https://upload.wikimedia.org/wikipedia/commons/d/d5/Rust_programming_language_black_logo.svg",
    '<h1>HTML Ipsum Presents</h1>

<p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>

<h2>Header Level 2</h2>

<ol>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ol>

<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote>

<h3>Header Level 3</h3>

<ul>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ul>

<pre><code>
#header h1 a {
  display: block;
  width: 300px;
  height: 80px;
}
</code></pre>',
    5
  );
-- SQLite
  -- Insert data into competances
INSERT INTO `competances` (nom, url, description, icon_path)
VALUES
  (
    "Python",
    "https://fr.wikipedia.org/wiki/Technologie",
    "Une techno avec un nom trop cool : Python",
    "https://upload.wikimedia.org/wikipedia/commons/1/10/Python_3._The_standard_type_hierarchy.png"
  ),
  (
    "Java",
    "https://fr.wikipedia.org/wiki/Technologie",
    "Une techno avec un nom trop cool : Java",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/Duke_%28Java_mascot%29_waving.svg/800px-Duke_%28Java_mascot%29_waving.svg.png"
  ),
  (
    "SQL",
    "https://fr.wikipedia.org/wiki/Technologie",
    "Une techno avec un nom trop cool : SQL",
    "https://upload.wikimedia.org/wikipedia/commons/c/c9/MariaDB_Logo.png"
  ),
  (
    "C",
    "https://fr.wikipedia.org/wiki/Technologie",
    "Une techno avec un nom trop cool : C",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/The_C_Programming_Language%2C_First_Edition_Cover_%282%29.svg/800px-The_C_Programming_Language%2C_First_Edition_Cover_%282%29.svg.png"
  ),
  (
    "Rust",
    "https://fr.wikipedia.org/wiki/Technologie",
    "Une techno avec un nom trop cool : Rusta",
    "https://upload.wikimedia.org/wikipedia/commons/d/d5/Rust_programming_language_black_logo.svg"
  );
-- SQLite
  -- Insert data into competances_projets
INSERT INTO `competances_projets` (idprojets, idcompetances)
VALUES
  (1, 1),
  (1, 2),
  (1, 3),
  (1, 4),
  (1, 5),
  (2, 2),
  (3, 3),
  (4, 4),
  (5, 5);