-- -----------------------------------------------------
-- Table `clients`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clients` (
  `idclients` INTEGER NOT NULL,
  `nom` VARCHAR(255) NULL,
  `website` VARCHAR(2000) NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`idclients`)
);
-- -----------------------------------------------------
-- Table `recomandations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `recomandations` (
  `idrecomandations` INTEGER NOT NULL,
  `titre` VARCHAR(100) NULL,
  `description` TEXT NULL,
  `idclients` INT NULL,
  `background_image_path` VARCHAR(2000) NULL,
  `background_color` VARCHAR(7) NULL,
  PRIMARY KEY (`idrecomandations`),
  CONSTRAINT `fk_recomandations_1` FOREIGN KEY (`idclients`) REFERENCES `clients` (`idclients`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
-- -----------------------------------------------------
-- Table `projets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projets` (
  `idprojets` INTEGER NOT NULL,
  `nom` VARCHAR(45) NULL,
  `url` VARCHAR(200) NULL,
  `description` TEXT NULL,
  `background_image_path` VARCHAR(2000) NULL,
  `article` MEDIUMTEXT NULL,
  `idclients` INT NULL,
  PRIMARY KEY (`idprojets`),
  CONSTRAINT `client` FOREIGN KEY (`idclients`) REFERENCES `clients` (`idclients`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
-- -----------------------------------------------------
-- Table `competances`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `competances` (
  `idcompetances` INTEGER NOT NULL,
  `nom` VARCHAR(300) NULL,
  `url` VARCHAR(2000) NULL,
  `description` TEXT NULL,
  `icon_path` VARCHAR(2000) NULL,
  PRIMARY KEY (`idcompetances`)
);
-- -----------------------------------------------------
-- Table `messages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `messages` (
  `idmessages` INT NOT NULL,
  `objet` VARCHAR(300) NULL,
  `corps` MEDIUMTEXT NULL,
  `email` VARCHAR(255) NULL,
  `messagescol` VARCHAR(45) NULL,
  `timestamp` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `prenom` VARCHAR(255) NULL,
  `nom` VARCHAR(255) NULL,
  PRIMARY KEY (`idmessages`)
);
-- -----------------------------------------------------
-- Table `competances_projets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `competances_projets` (
  `idcompetances_projets` INTEGER NOT NULL,
  `idprojets` INT NULL,
  `idcompetances` INT NULL,
  PRIMARY KEY (`idcompetances_projets`),
  CONSTRAINT `fk_competances_projets_1` FOREIGN KEY (`idcompetances`) REFERENCES `competances` (`idcompetances`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_competances_projets_2` FOREIGN KEY (`idprojets`) REFERENCES `projets` (`idprojets`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
-- -----------------------------------------------------
-- Table `requete_recommandation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `requete_recommandation` (
  `idrecomandations` INTEGER NOT NULL,
  `titre` VARCHAR(100) NULL,
  `description` TEXT NULL,
  `email` VARCHAR(255) NULL,
  `background_image_path` VARCHAR(2000) NULL,
  `background_color` VARCHAR(7) NULL,
  `nom_client` VARCHAR(255) NULL,
  PRIMARY KEY (`idrecomandations`)
);