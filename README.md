# Projet de Campus Contest 2019
## Membres :
* Alix Biton
* Tanguy Pichon

## Maquette
* https://www.figma.com/file/hImpuM7HqG6FHFfDdQ0Fct/Campus-Constest?node-id=0%3A1

## Gestion de projets
* https://docs.google.com/spreadsheets/d/1iPAWHjSUZIh6BjqwvT4LJKUdlFESKd6WzqBw131y1C4/edit?usp=sharing

## Technologies utilisées
* GitLab
  * Gestion de version
  * Gestion de projet ( Issues, Kanban )

## Installation des dépendances
Création d'un virtualenv :
`python3 -m venv env`

Activation de l'environement :
`source env/bin/activate`

Installation des dépendances :
`pip install -r requierments.txt`

## Génération de la base de donnée

Création des tables :
`sqlite3 db.db < ./DB/scripts/sqlite_database.sql`

Import des données de test :
`sqlite3 db.db < ./DB/scripts/sqlite_data.sql`

## Lancement du serveur web de développement

`flask run`

Ouvrir votre navigateur a l'adresse indiquée par la commande
